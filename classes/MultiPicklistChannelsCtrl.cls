/**
 * Created by Administrator on 12.12.2018.
 */

public with sharing class MultiPicklistChannelsCtrl {
    @AuraEnabled
    public static List <String> getSelectOptions() {
        List <String> allOptions = new list <String>();
        for(PushTopic topic: [SELECT Id, Name  From PushTopic WHERE IsActive=true]) {
            allOptions.add(topic.Name);
        }
        allOptions.sort();
        return allOptions;
    }
}