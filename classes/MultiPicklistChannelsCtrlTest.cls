/**
 * Created by Administrator on 28.12.2018.
 */
@isTest
public with sharing class MultiPicklistChannelsCtrlTest {

    @isTest
    static void test_getSelectOptions() {

        List<PushTopic> list1 = new List<PushTopic>();
        for (Integer i = 0; i < 3; i++) {
            PushTopic topic = new PushTopic();
            topic.ApiVersion = 43;
            topic.Name = 'AccountPT' + i;
            topic.Query = 'SELECT Id , Name FROM Account';
            topic.NotifyForFields = 'All';
            topic.NotifyForOperationCreate = true;
            topic.NotifyForOperationUpdate = false;
            topic.Description = 'lorem ipsum' + i;
            list1.add(topic);
        }
        insert list1;

        PushTopic topic = new PushTopic();
        topic.IsActive = false;
        topic.ApiVersion = 43;
        topic.Name = 'Contact-PT';
        topic.Query = 'SELECT Id , Phone FROM Contact';
        topic.NotifyForFields = 'All';
        topic.NotifyForOperationCreate = true;
        topic.NotifyForOperationUpdate = true;
        topic.NotifyForOperationDelete = true;
        topic.Description = 'lorem ipsum';
        insert topic;

        Test.startTest();
        List<String> listTest = MultiPicklistChannelsCtrl.getSelectOptions();
        Test.stopTest();
        System.assertNotEquals(null, listTest);
        System.assertEquals(3, listTest.size());
    }
}