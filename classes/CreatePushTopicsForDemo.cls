/**
 * Created by Administrator on 28.12.2018.
 */

public with sharing class CreatePushTopicsForDemo {

    static String str = 'Demo';
    static String str2 = '';
    public static void generatePT() {
        if (Test.isRunningTest()) {
            str = 'Test';
            str2 = 'Test';
        }

        List<PushTopic> listPT = new List<PushTopic>();
        PushTopic topic1 = new PushTopic();
        topic1.ApiVersion = 43;
        topic1.Name = 'Account.not' + str;
        topic1.Query = 'SELECT Id, Name FROM Account';
        topic1.NotifyForFields = 'Referenced';
        topic1.NotifyForOperationCreate = true;
        topic1.NotifyForOperationUpdate = true;
        topic1.NotifyForOperationDelete = true;
        topic1.Description = 'lorem ipsum' + str;

        PushTopic topic2 = new PushTopic();
        topic2.ApiVersion = 43;
        topic2.Name = 'LeadPushTopic' + str;
        topic2.Query = 'SELECT Id, Name, Company, Title,Phone  FROM Lead';
        topic2.NotifyForFields = 'Referenced';
        topic2.NotifyForOperationCreate = true;
        topic2.NotifyForOperationUpdate = true;
        topic2.NotifyForOperationDelete = true;

        PushTopic topic3 = new PushTopic();
        topic3.ApiVersion = 43;
        topic3.Name = 'AccountSpy' + str2;
        topic3.Query = 'SELECT Id, Name, Phone, Website FROM Account';
        topic3.NotifyForFields = 'Referenced';
        topic3.NotifyForOperationCreate = true;
        topic3.NotifyForOperationUpdate = true;
        topic3.NotifyForOperationDelete = true;

        PushTopic topic4 = new PushTopic();
        topic4.ApiVersion = 43;
        topic4.Name = 'ContactPushTopic' + str;
        topic4.Query = 'SELECT Id, FirstName, Title, Phone  FROM Contact';
        topic4.NotifyForFields = 'All';
        topic4.NotifyForOperationCreate = true;
        topic4.NotifyForOperationUpdate = true;
        topic4.NotifyForOperationDelete = true;

        listPT.add(topic1);
        listPT.add(topic2);
        listPT.add(topic3);
        listPT.add(topic4);

        insert listPT;
    }
}