/**
 * Created by Administrator on 28.12.2018.
 */
@isTest
public with sharing class CreatePushTopicsForDemoTest {

    @isTest
    static void testGeneratePT() {

        Test.startTest();
        CreatePushTopicsForDemo.generatePT();
        Test.stopTest();

        List<PushTopic> topics = [SELECT Id FROM PushTopic];
        System.assertEquals(4, topics.size());
    }
}