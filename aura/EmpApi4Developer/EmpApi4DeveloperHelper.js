/**
 * Created by Administrator on 12.12.2018.
 */
({
    subscribe: function (component, event, helper) {
        var channels = component.get("v.channelsName");
        if (!!channels) {
            var empApi = component.find("empApi");
            //fetch latest events if -1
            var replayId = component.get("v.replayId");

            var subscribeCallback = function (message) {
                //Fire the component event to notify parent component
                var messageEvent = component.getEvent("onCRUDEmpEvent");
                if (messageEvent != null) {
                    messageEvent.setParam("empData", message.data);
                    messageEvent.fire();
                }
                switch (message.data.event.type) {
                    case "created":
                        var messageEvent = component.getEvent("onCreateEmpEvent");
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", message.data);
                            messageEvent.fire();
                        }
                        break;
                    case "updated":
                        var messageEvent = component.getEvent("onUpdateEmpEvent");
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", message.data);
                            messageEvent.fire();
                        }
                        break;
                    case "deleted":
                        var messageEvent = component.getEvent("onDeleteEmpEvent");
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", message.data);
                            messageEvent.fire();
                        }
                        break;
                    case "undeleted":
                        var messageEvent = component.getEvent("onUndeleteEmpEvent");
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", message.data);
                            messageEvent.fire();
                        }
                        break;
                }
                //Display event data in browser console
                console.log("Received [" + message.channel + " : " + message.data.event.type +
                    "] payload=" + JSON.stringify(message.data.sobject));
            }.bind(this);


            // Register error listener and pass in the error handler function
            empApi.onError($A.getCallback(function (error) {
                var messageEvent = component.getEvent("onError");
                if (messageEvent != null) {
                    messageEvent.setParam("empData", error);
                    messageEvent.fire();
                }
                console.error('EMP API error: ', JSON.stringify(error));
            }));

            var messageEvent = component.getEvent("onSubscribe");
            // Subscribe to the channel and save the returned subscription object.
            for (let i = 0; i < channels.length; i++) {
                empApi.subscribe(channels[i], replayId, subscribeCallback).then(function (value) {
                    var subscriptions = component.get("v.subscriptions");
                    // subscriptions = [];
                    subscriptions.push(value);
                    component.set("v.subscriptions", subscriptions);

                    console.log(i + " Subscribed to channel " + channels[i]);

                    if (i == (channels.length - 1)) {
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", {"status": "SUCCESS", "subscriptions": subscriptions});
                            messageEvent.fire();
                        }
                        if (!!component.get('v.enableToasterNotification')) {
                            helper.runToaster(component, event, helper, 1, channels);
                        }
                    }
                }).catch(function (value) {
                    if (messageEvent != null) {
                        messageEvent.setParam("empData", {"status": "ERROR"});
                        messageEvent.fire();
                    }
                    if (!!component.get('v.enableToasterNotification')) {
                        helper.runToaster(component, event, helper, 1, channels, value);
                    }
                });
            }
        }
    },
    unsubscribe: function (component, event, helper) {
        var channels = component.get("v.channelsName");
        if (!!channels) {
            var empApi = component.find("empApi");
            // Callback function to be passed in the unsubscribe call.
            var unsubscribeCallback = function (message) {
                console.log("Unsubscribed from channel " + message.subscription);
                // component.set('v.toDoUnsubscription', false);
            }.bind(this);

            // Error handler function that prints the error to the console.
            var errorHandler = function (message) {
                console.log("Received error ", message);
            }.bind(this);

            // Object that contains subscription attributes used to
            // unsubscribe.
            var subscriptions = [];
            channels.forEach(function(ch, i, arr) {
                component.get("v.subscriptions").forEach(function(sub, i, arr) {
                    if (ch === sub["channel"]) {
                        subscriptions.push({"id": sub["id"],
                            "channel": sub["channel"]});
                    }
                });
            });

            // Register error listener and pass in the error handler function
            empApi.onError($A.getCallback(function (error) {
                var messageEvent = component.getEvent("onError");
                if (messageEvent != null) {
                    messageEvent.setParam("empData", error);
                    messageEvent.fire();
                }
                console.error('EMP API error: ', JSON.stringify(error));
            }));
            var messageEvent = component.getEvent("onUnsubscribe");

            // Unsubscribe from the channel using the sub object.
            for (let i = 0; i < subscriptions.length; i++) {
                empApi.unsubscribe(subscriptions[i], unsubscribeCallback).then(function (value) {
                    if (i == (subscriptions.length - 1)) {
                        if (messageEvent != null) {
                            messageEvent.setParam("empData", {"status": "SUCCESS", "subscriptions": subscriptions});
                            messageEvent.fire();
                        }
                        if (!!component.get('v.enableToasterNotification')) {
                            helper.runToaster(component, event, helper, 0, channels);
                        }
                    }

                }).catch(function (value) {
                    if (messageEvent != null) {
                        messageEvent.setParam("empData", {"status": "ERROR"});
                        messageEvent.fire();
                    }
                    if (!!component.get('v.enableToasterNotification')) {
                        helper.runToaster(component, event, helper, 0, channels, value);
                    }
                });
            }
        }
    },
    runToaster: function (component, event, helper, ifSubscr, channels, error) {

        var eventType = (ifSubscr === 1) ? 'subscribed' : 'unsubscribed';

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: 'Info Message',
            message: (!!error) ? "You haven't been" + eventType : "You have been " + eventType + " -- channel(s): " + channels,
            duration: '4000',
            key: 'info_alt',
            type: (!!error) ? 'warning' : 'info',
            mode: 'dismissible'
        });
        toastEvent.fire();
    }
})