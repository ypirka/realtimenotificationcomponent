/**
 * Created by posivetsa on 12/10/2018.
 */
({
    /**
     * This function calls subscribe method of empApi component
     * to receive events
     * */
    subscribe: function(component, event, helper) {
        helper.subscribe(component, event, helper);
    },
    /**
     * This function will be fired once the component is destroyed
     * It will call unsubscribe method of empApi component
     * */
    unsubscribe : function (component, event, helper) {
        helper.unsubscribe(component, event, helper);
    },
    /**
     * This function will be fired if Developer will change attribute v.toDoSubscription from False to True
     * It will call subscribe method of empApi component
     * */
    doSubscribe: function (component, event, helper) {
        if(component.get('v.toDoSubscription')) {
            helper.subscribe(component, event, helper);
            component.set('v.toDoSubscription', false);
        }
    },
    /**
     * This function will be fired if Developer will change attribute v.toDoUnsubscription from False to True
     * It will call unsubscribe method of empApi component
     * */
    doUnsubscribe : function (component, event, helper) {
        if(component.get('v.toDoUnsubscription')) {
            helper.unsubscribe(component, event, helper);
//            component.set('v.toDoUnsubscription', false);
        }
    }
})