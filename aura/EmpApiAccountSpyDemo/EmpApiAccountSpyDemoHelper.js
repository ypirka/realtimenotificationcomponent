/**
 * Created by posivetsa on 12/12/2018.
 */
({
    getDefinedToasterVariant: function (component, event, helper, eventType) {
        var variant;
        switch (eventType) {
            case "created":
                variant = 'success';
                break;
            case "updated":
                variant = 'info';
                break;
            case "undeleted":
                variant = 'warning';
                break;
            case "deleted":
                variant = 'error';
                break;
        }
        return variant;
    },
    runToaster: function (component, event, helper, param) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: 'Info Message',
            message: 'Account ' + param.sobject.Name +
            ' with Id  :' + param.sobject.Id + ' is ' + param.event.type,
            duration: ' 8000',
            key: 'info_alt',
            type: helper.getDefinedToasterVariant(component, event, helper, param.event.type),
            mode: 'dismissible'
        });
        toastEvent.fire();
    }
})