/**
 * Created by posivetsa on 12/6/2018.
 */
({
    handleCreate : function (component, event, helper) {
        const param = event.getParam('empData');
        document.querySelector('#output').innerHTML +=
            '<p class="slds-p-horizontal_medium">- Account ' + param.sobject.Name +
            ' with Id ' + param.sobject.Id + ' is ' + param.event.type + '!!</p>';
        console.log('Account "${param.sobject.Name}" with Id ${param.sobject.Id} is ${param.event.type}!!');

        helper.runToaster(component, event, helper, param);
    },
    handleDelete: function (component, event, helper) {
        const param = event.getParam('empData');
        document.querySelector('#output').innerHTML +=
            '<p class="slds-p-horizontal_medium">- Account  with Id ' + param.sobject.Id + ' was ' + param.event.type + '!!</p>';
        console.log('Account "${param.sobject.Name}" with Id ${param.sobject.Id} is ${param.event.type}!!');

        helper.runToaster(component, event, helper, param);
    },
    doAction : function (component, event, helper) {
        if(component.get('v.doUnsubsc') == false && component.get('v.doSubsc') == false) {
            component.set('v.doUnsubsc', true);

        } else  if (component.get('v.doUnsubsc') == true && component.get('v.doSubsc') == false) {
            component.set('v.doSubsc', true);
        }
    },
    handleSubscribe: function (component, event, helper) {
        const param = event.getParam('empData');
        if(param.status === 'SUCCESS') {
            component.set('v.label', 'UNSUBSCRIBE');
            component.set('v.variant', 'destructive');
            component.set('v.doUnsubsc', false);
            component.set('v.doSubsc', false);
        }
    },
    handleUnsubscribe: function (component, event, helper) {
        const param = event.getParam('empData');
        if (param.status === 'SUCCESS') {
            component.set('v.label', 'SUBSCRIBE');
            component.set('v.variant', 'success');
        }
    }
})