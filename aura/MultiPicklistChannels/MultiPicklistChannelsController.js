/**
 * Created by Administrator on 12.12.2018.
 */
({
    doInit: function (component, event, helper) {
        component.set('v.channels', ['/topic/LeadPushTopic', '/topic/ContactPushTopic']);
        var action = component.get("c.getSelectOptions");
        var opts = [];
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.listAvailableChannels", opts);
            } else {
                alert('Callback Failed...');
            }
        });
        $A.enqueueAction(action);
    },
    handleChange: function (component, event) {
        // get the updated/changed values
        var selectedOptionsList = event.getParam("value");
        // get the updated/changed source
        var targetName = event.getSource().get("v.name");
        var rest = [];
        component.get("v.previousSelectedChannels").forEach(function (prev, i, arr) {
            if (selectedOptionsList.indexOf(prev) < 0) {
                rest.push(prev);
            }
        });
        component.set('v.unsubscribtionChannels', rest);
        // update the selected itmes
        if (targetName == 'Channels') {
            component.set("v.previousSelectedChannels", selectedOptionsList);
        }
    },
    handleCRUD: function (component, event, helper) {
        const param = event.getParam('empData');

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: 'Info Message',
            message: 'Object ' + param.sobject.Name +
            ' with Id  :' + param.sobject.Id + ' is ' + param.event.type,
            duration: ' 8000',
            key: 'info_alt',
            type: helper.getDefinedToasterVariant(component, event, helper, param.event.type),
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
    subscribe: function (component, event, helper) {
        // get selected items on button click
        var selected = component.get("v.selectedChannelsItems");
        var channels = [];
        for (var i = 0; i < selected.length; i++) {
            channels.push('/topic/' + selected[i]);
        }
        component.set("v.channels", channels);

        component.set("v.doSubsc", true);
    },
    unsubscribe: function (component, event, helper) {
        var selected = component.get("v.unsubscribtionChannels");
        var channels = [];

        for (var i = 0; i < selected.length; i++) {
            channels.push('/topic/' + selected[i]);
        }
        component.set("v.channels", channels);
        component.set("v.doUnsubsc", true);
        component.set("v.channels",  component.get("v.previousSelectedChannels"));
        component.set("v.doUnsubsc", false);
    }
})