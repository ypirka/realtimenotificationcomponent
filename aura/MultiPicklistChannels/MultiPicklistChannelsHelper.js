/**
 * Created by posivetsa on 12/13/2018.
 */
({
    getDefinedToasterVariant: function (component, event, helper, eventType) {
        var variant;
        switch (eventType) {
            case "created":
                variant = 'success';
                break;
            case "updated":
                variant = 'info';
                break;
            case "undeleted":
                variant = 'warning';
                break;
            case "deleted":
                variant = 'error';
                break;
        }
        return variant;
    }
})